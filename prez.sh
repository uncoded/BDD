#!/bin/bash

SOURCEDIR="course"
EXPORTDIR="public/presentation"
mkdir -p ${EXPORTDIR}

#############
# REVEAL.JS #
#############

ARCHIVE="master.zip"
REVEALDIR="reveal.js-master"
wget https://github.com/hakimel/reveal.js/archive/${ARCHIVE}
unzip ${ARCHIVE}
cp -R ${REVEALDIR}/dist ${EXPORTDIR}/
cp -R ${REVEALDIR}/plugin ${EXPORTDIR}/
cp -R ${SOURCEDIR}/presentation.html ${EXPORTDIR}/index.html

# cleanup
rm -rf ${ARCHIVE} ${REVEALDIR}
