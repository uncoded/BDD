#!/bin/bash

STYLESHEET="wizdom"
STYLESDIR="stylesheets"
SOURCEDIR="course"
IMAGESDIR="resources"
EXPORTDIR="public"
DEFAULTLG="fr"

# compile sassy stylesheets (committed in ./sass folder)
# into CSS stylesheets which will be created in $STYLESDIR
# tose locations are set in ./config.rb
compass compile
# don't forget to copy resources
mkdir -p ${EXPORTDIR}
cp -r resources ${EXPORTDIR}/

###################
# COURSE MATERIAL #
###################
# generate .html page in each language output folder
LOCALE="fr"
asciidoctor course/bases-de-donnees.adoc \
         -a stylesheet="${STYLESHEET}.css"    \
         -a stylesdir="../${STYLESDIR}"       \
         -a imagesdir="${IMAGESDIR}"          \
         -o ${EXPORTDIR}/${LOCALE}/index.html
LOCALE="en"
asciidoctor course/databases.adoc \
         -a stylesheet="${STYLESHEET}.css"    \
         -a stylesdir="../${STYLESDIR}"       \
         -a imagesdir="../${IMAGESDIR}"       \
         -o ${EXPORTDIR}/${LOCALE}/index.html
# dupe default language
cp ${EXPORTDIR}/${DEFAULTLG}/index.html ${EXPORTDIR}/
